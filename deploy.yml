include:
    # workflow rules to prevent duplicate detached pipelines
    -   template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
    # auto devops build
    -   template: 'Jobs/Build.gitlab-ci.yml'

stages:
    - build
    - test
    - provision
    - deploy
    - destroy

variables:
    TF_VAR_AWS_ACCESS_KEY: ${AWS_ACCESS_KEY}
    TF_VAR_AWS_SECRET_KEY: ${AWS_SECRET_KEY}
    TF_VAR_AWS_REGION: ${AWS_REGION}
    TF_VAR_ENVIRONMENT_NAME: ${CI_PROJECT_PATH_SLUG}_${CI_PROJECT_ID}_${CI_COMMIT_REF_SLUG}
    TF_VAR_SHORT_ENVIRONMENT_NAME: ${CI_PROJECT_ID}-${CI_COMMIT_REF_SLUG}
    TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_COMMIT_REF_SLUG}

cache:
    paths:
        - .terraform

.needs_aws_vars:
    rules:
        -   if: '$AWS_ACCESS_KEY && $AWS_SECRET_KEY && $AWS_REGION'
            when: on_success
        -   when: never

.needs_aws_vars_for_manual:
    rules:
        -   if: '$AWS_ACCESS_KEY && $AWS_SECRET_KEY && $AWS_REGION'
            when: manual
        -   when: never

terraform_plan:
    stage: test
    image: registry.gitlab.com/gitlab-org/5-minute-production-app/deploy-template/master
    extends: .needs_aws_vars
    resource_group: terraform
    before_script:
        - cp /*.tf .
        - cp /deploy.sh .
    script:
        - gitlab-terraform init
        - gitlab-terraform plan
        - gitlab-terraform plan-json
    artifacts:
        paths:
            - plan.cache
        reports:
            terraform: plan.json

terraform_apply:
    stage: provision
    image: registry.gitlab.com/gitlab-org/5-minute-production-app/deploy-template/master
    extends: .needs_aws_vars
    resource_group: terraform
    before_script:
        - cp /*.tf .
        - cp /deploy.sh .
    script:
        - gitlab-terraform apply
    artifacts:
        paths:
            - plan.cache
        reports:
            terraform: plan.json

deploy:
    stage: deploy
    image: registry.gitlab.com/gitlab-org/5-minute-production-app/deploy-template/master
    extends: .needs_aws_vars
    resource_group: deploy
    before_script:
        - cp /*.tf .
        - cp /deploy.sh .
    script:
        - ./deploy.sh
    artifacts:
        reports:
            dotenv: deploy.env
    environment:
        name: $CI_COMMIT_REF_SLUG
        url: http://$DYNAMIC_ENVIRONMENT_URL
        on_stop: terraform_destroy

terraform_destroy:
    stage: destroy
    image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
    extends: .needs_aws_vars_for_manual
    script:
        - gitlab-terraform destroy -auto-approve
    environment:
        name: $CI_COMMIT_REF_SLUG
        action: stop
